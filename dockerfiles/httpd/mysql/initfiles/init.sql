ALTER USER 'root'@'localhost' IDENTIFIED BY 'pass';
CREATE USER 'root'@'%' IDENTIFIED BY 'pass';
GRANT ALL ON *.* TO 'root'@'%';
FLUSH PRIVILEGES;
