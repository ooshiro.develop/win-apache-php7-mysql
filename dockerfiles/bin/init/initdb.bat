mysqld --initialize-insecure --user=mysql
mysqld --install MYSQL80 --defaults-file="c:\mysql\my.ini"
NET START MYSQL80
mysql -u root --skip-password < "c:\mysql\initfiles\init.sql"
mysql -u root -p"pass" mysql < "c:\mysql\initfiles\timezone_posix.sql"

echo [mysqld] >> c:\mysql\my.ini
echo default-time-zone = 'Asia/Tokyo' >> c:\mysql\my.ini

NET STOP MYSQL80
